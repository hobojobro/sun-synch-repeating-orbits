# Sun-synch repeating orbits

Recruitment task for Flight Dynamics Engineer position at ACME Corp.

Two part task:
1. Find number of unique sun-synchronous orbits with repeating ground tracks from 540-600 km in altitude and max repeat cycle of 20 days.
2. Find duration of low-thrust spiral transfer from TLE to 1 day repeat cycle altitude from part 1.
