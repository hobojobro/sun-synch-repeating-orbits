import math
import matplotlib.pyplot as plt
from matplotlib.ticker import StrMethodFormatter
import numpy as np
from sgp4.api import Satrec, WGS84

#########################
# part 1
#########################

# constants
R_earth = 6378.137 #km
mu = 398600.4418 #km^3/sec^2
J2 = 0.00108262668
node_rate = 2*math.pi/(365.2358*86400) #rad/sec, RAAN rate for sun-synchronicity
rad2deg = 180/math.pi

# inputs
alt_minmax = [540, 600] #km, min and max
max_repeat = 20 #days
out_file = "results.txt"

# altitude to semimajor axis (assumes circular orbit) and period
sma_minmax = [a + R_earth for a in alt_minmax] #km
period_minmax = [2*math.pi*math.sqrt(a**3/mu) for a in sma_minmax] #sec

orbits = [[],[],[],[],[],[]]
for k in range(1, max_repeat+1):
    # precise number of orbits corresponding to min/max sma for current repeat period
    revs = [k/p*86400 for p in period_minmax]
    # get range of all integer values between min and max
    revs_int = [math.ceil(revs[1]), math.floor(revs[0])]
    for j in range(revs_int[0], revs_int[1]+1):
        # only calculate/save unique orbits
        if not j/k in orbits[5]:
            # calculate orbit for this repeat cycle
            period = k/j*86400 #sec
            sma = (mu*(period/(2*math.pi))**2)**(1/3)
            inc = math.acos(-2*node_rate*sma**(7/2)/(3*J2*(R_earth**2)*math.sqrt(mu))) #rad
            # save altitude, inclination, period, cycle params for each orbit
            orbits[0].append(sma - R_earth) #km
            orbits[1].append(inc*rad2deg) #deg
            orbits[2].append(period) #sec
            orbits[3].append(k)
            orbits[4].append(j)
            orbits[5].append(j/k)
n_orbits = len(orbits[0])

# write results
f_out = open(out_file, 'w')
f_out.write(f"Number of unique repeating orbits (n_orbits) = {n_orbits:d}\n")
f_out.write("\n")
f_out.write("Parameters for each orbit:\n")
f_out.write("Alt. [km]\tInc. [deg]\tPeriod [sec]\n")
for i in range(n_orbits):
    line = f"{orbits[0][i]:.3f}\t\t{orbits[1][i]:.3f}\t\t{orbits[2][i]:.1f}\n"
    f_out.write(line)
# plot repeat cycle vs height
fig, ax = plt.subplots()
plt.plot(orbits[0], orbits[3], 'b.')
plt.xlabel("Altitude [km]")
plt.ylabel("Repeat Cycle [days]")
ax.set_yticks(np.arange(0, max_repeat+1, 2))
plt.gca().yaxis.set_major_formatter(StrMethodFormatter('{x:,.0f}')) # No decimal places
plt.savefig("repeating_orbits.png")

#########################
# part 2
#########################

# inputs
mass = 80 #kg
thrust = 0.001 #N
tle_file = "ICEYE-X2.tle"
sma_final = orbits[0][0] + R_earth #km

# load TLE and get semimajor axis
f_tle = open(tle_file, "r")
lines = f_tle.readlines()
f_tle.close()
tle_state = Satrec.twoline2rv(lines[1], lines[2])
sma_initial = tle_state.a*R_earth #km (SGP4 uses units of Earth radii for distance)

# save time (in days) to transfer from TLE orbit to 1 day repeat ground track altitude (no inc or ecc correction)
t_transfer = abs(math.sqrt(mu)/(thrust/mass)*(1/math.sqrt(sma_initial) - 1/math.sqrt(sma_final))/86400) #days

f_out.write("\n")
f_out.write(f"ICEYE-X2 initial height = {sma_initial-R_earth:.3f} km\n")
f_out.write(f"1 day repeating ground track height = {sma_final-R_earth:.3f} km\n")
f_out.write(f"Spiral transfer duration (t_transfer) = {t_transfer:.6f} days = {t_transfer*86400:.1f} sec\n")
f_out.close()